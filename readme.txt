Created by Michael Salata
msalata89@gmail.com

Purpose:  Adds easy hotkeys adjust Sound on any standard keyboard.

Binds:
	Ctrl + Up Arrow	 		:	Increase Sound Volume
	Ctrl + Down Arrow 		:	Decrease Sound Volume
	Right Alt + Down Arrow	:	Mute Sound
	Right Ctrl + Left Arrow		:	Pause Media Player
	Right Ctrl + Right Arrow	:	Next Song (Media Player)
	
NOTE: You may have to set the .exe to run in Administrative mode.

Setup: Do One of the Below...
	A: Double Click the .exe file
	
	B: Double Click the .ahk file with Autohotkey installed
	
	C:	Add the .exe to your Startup folder.
		The Startup folder is located in %AppData%\Roaming\Microsoft\Windows\Start Menu\Programs folder.
		AppData folder can also be found by typing  %AppData% into your Windows Start Search bar.
		Add a shortcut to the .exe in the Startup folder
		Alternatively, you can move the .exe into the Startupfolder
		
	D: Create a Task that Runs on Startup
		Open Your Task Scheduler (type Task Scheduler into your windows search bar)
		Click - Create Task...  on the right hand side
			Navigate to the Actions Tab
				Click - New...
				Set Action to "Start a program"
				Set "Program/script:" to the location of the .exe
			Navigate to the Actions Tab
				Click - New...
				Set "Begin the task:" to  "At log on" or "At startup"