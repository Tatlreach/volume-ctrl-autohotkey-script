;	Created by Michael Salata
;	msalata89@gmail.com
;	Purpose: Make sound easily adjusted on any standard keyboard.
;	Compile this to .exe and automatically run at startup for it to function in the background.
;	can also be run if as a  .ahk file if Autohotkey is installed on your computer

	
^Up::								;	Ctrl+Up Arrow
	Send {Volume_Up 1}
	return
	
^Down::							;	Ctrl+Down Arrow
	Send {Volume_Down 1}
	return
	
RAlt & Down::
	Send {Volume_Mute}
	return


;	Functionality below is restricted to the Right Control Modifier
;	This was done to allow the use of the Left Control Button and the Arrow keys
;	LCtrl + LeftArrow/RightArrow is useful for text editing (jumping between words)

;	It is also more comfortable to hit the hotkey with 1 hand.

RControl & Left::
	Send {Media_Play_Pause}
	return
	
~RControl & Right::
	Send {Media_Next}
	return


;	also makes sound easily adjusted on any standard keyboard with 1 hand

~Numpad0 & NumpadAdd::			;	~ means it doesn't block default functionality
	Send {Volume_Up 1}
	return
	
~Numpad0 & NumpadSub::			;	~ means it doesn't block default functionality
	Send {Volume_Down 1}
	return

